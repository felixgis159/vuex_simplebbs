const apiRoot = 'http://localhost:10080';

const api = {
	login: apiRoot + "/login",
	register: apiRoot + "/register",
	getHomeProfile: apiRoot + "/profile",
	getHomeBanners: apiRoot + "/homebanners?pos=home_top&type=app",
	getHomeBoards: apiRoot + "/homeboards",
	getHomeRecommendTopics: apiRoot + "/hometopics",
	getTopics: apiRoot + "/board/",
	createTopic: apiRoot + "/thread"
}

export default api;