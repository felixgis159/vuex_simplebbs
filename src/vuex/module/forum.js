import * as types from '../mutation-types';
import Vue from 'vue';
import VueResouce from 'vue-resource';
import nativeToast from 'native-toast';
import jic from '../../helper/image';

Vue.use(VueResouce);

const state = {
	topics: [],
	boardInfo: {
		bid: 0,
		title: ""
	},
	topicTitle: "",
	topicMessage: ""
}

const mutations = {
	[types.GET_TOPIC_LIST](state, payload) {
		state.topics = payload;
	},
	updateTitle(state, payload) {
		state.topicTitle = payload;
	},
	updateMessage(state, payload) {
		state.topicMessage = payload;
	},
	getBordInfo(state, payload) {
		state.boardInfo.bid = payload.bid;
		state.boardInfo.title = payload.title;
	}
}

const actions = {
	getTopics({ commit, state }, payload) {
		var apiPath = payload.apiPath;
		Vue.http.get(apiPath).then((response) => {
			commit(types.GET_TOPIC_LIST, response.body.result);
		});
	},
	createTopic({ commit, state }, payload) {
		var apiPath = payload.apiPath;
		Vue.http.post(apiPath, {
			bid: state.boardInfo.bid,
			subject: payload.subject,
			message: payload.message,
			orgAttachs: JSON.stringify(payload.orgAttachs)
		}).then((response) => {
			if (response.body.errorCode == 0) {
				commit('updateTitle', "");
				commit('updateMessage', "");
				payload.router.push({
					name: "threads",
					params: {
						bid: state.boardInfo.bid
					},
					query: {
						title: state.boardInfo.title
					}
				});
			} else {
				nativeToast({
					message: response.body.message,
					position: 'bottom',
					timeout: 2000
				});
			}
		});
	}
}

const getters = {
	topicsOfBoard: state => {
		return state.topics;
	},
	boardInfo: state => {
		return state.boardInfo;
	}
}

export default {
	state: state,
	mutations: mutations,
	actions: actions,
	getters: getters
}