import * as types from '../mutation-types';

const state = {
	currTab: false
}

const mutations = {

}

const actions = {

}

const getters = {

}

export default {
	state: state,
	mutations: mutations,
	actions: actions,
	getters: getters
}