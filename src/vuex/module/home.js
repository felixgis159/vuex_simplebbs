import * as types from '../mutation-types';
import Vue from 'vue';
import VueResouce from 'vue-resource';
import nativeToast from 'native-toast';
import token from '../../helper/token';

Vue.use(VueResouce);

const state = {
    banners: [],
    boards: [],
    topics: [],
    currTab: true
}

const mutations = {
    [types.GET_HOME_BANNER](state, payload) {
        state.banners = payload;
    },
    [types.GET_HOME_BOARDS](state, payload) {
        state.boards = payload;
    },
    [types.GET_HOME_RECOMMEND_TOPICS](state, payload) {
        state.topics = payload;
    },
    [types.LOGIN](state, payload) {
        state.errorMsg = payload;
    },
    loadPage(state, payload) {
        var key = payload.currTab;
        if (key == "hot") {
            payload.store.state.home.currTab = true;
            payload.store.state.favorite.currTab = false;
            payload.store.state.search.currTab = false;
            payload.store.state.profile.currTab = false;
            payload.router.push({
                path: "/",
                name: "home"
            });
        } else if (key == "favorite") {
            payload.store.state.home.currTab = false;
            payload.store.state.favorite.currTab = true;
            payload.store.state.search.currTab = false;
            payload.store.state.profile.currTab = false;
            payload.router.push({
                path: "/favorite",
                name: "favorite"
            });
        } else if (key == "search") {
            payload.store.state.home.currTab = false;
            payload.store.state.favorite.currTab = false;
            payload.store.state.search.currTab = true;
            payload.store.state.profile.currTab = false;
            payload.router.push({
                path: "/search",
                name: "search"
            });
        } else if (key == "profile") {
            payload.store.state.home.currTab = false;
            payload.store.state.favorite.currTab = false;
            payload.store.state.search.currTab = false;
            payload.store.state.profile.currTab = true;
            payload.router.push({
                path: "/profile",
                name: "profile"
            });
        }
    }
}

const actions = {
    getHomeBanner({ commit, state }, apiPath) {
        Vue.http.get(apiPath).then((response) => {
            commit(types.GET_HOME_BANNER, response.body.result);
        });
    },
    getHomeTopic({ commit, state }, apiPath) {
        Vue.http.get(apiPath).then((response) => {
            commit(types.GET_HOME_RECOMMEND_TOPICS, response.body.result);
        });
    },
    getHomeBoard({ commit, state }, apiPath) {
        Vue.http.get(apiPath).then((response) => {
            commit(types.GET_HOME_BOARDS, response.body.result);
        });
    },
    login({ commit, state }, payload) {
        Vue.http.post(payload.apiPath, { account: payload.account, password: payload.password }).then((response) => {
            if (response.body.errorCode == 0) {
                token.setToken(response.body.result.token);
                payload.router.push({
                    path: "/",
                    name: "home"
                })
            } else {
                nativeToast({
                    message: response.body.message,
                    position: 'bottom',
                    timeout: 2000
                });
            }
        });
    },
    register({ commit, state }, payload) {
        Vue.http.post(payload.apiPath, { account: payload.account, password: payload.password }).then((response) => {
            if (response.body.errorCode == 0) {
                payload.router.push({
                    path: "/",
                    name: "home"
                })
            } else {
                nativeToast({
                    message: response.body.message,
                    position: 'bottom',
                    timeout: 2000
                });
            }
        });
    }
}

export default {
    state: state,
    mutations: mutations,
    actions: actions
}