import Vue from 'vue';
import Vuex from 'vuex';

import profile from './module/profile';
import message from './module/message';
import forum from './module/forum';
import home from './module/home';
import search from './module/search';
import favorite from './module/favorite';

Vue.use(Vuex);

const store = new Vuex.Store({
	strict: true
});

store.registerModule('home', home);
store.registerModule('forum', forum);
store.registerModule('message', message);
store.registerModule('profile', profile);
store.registerModule('favorite', favorite);
store.registerModule('search', search);


export default store