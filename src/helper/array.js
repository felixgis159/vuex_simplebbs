const arrayHelper ={
	unique(arr){
		var ids=[];
		var names=[];
		arr.map(function(obj) {
			if(names.indexOf(obj.name)>=0){
				ids.push(obj.id);
			}else{
				names.push(obj.name);
			}
		});

		for (var i = 0; i < arr.length; i++) {
			for (var j = 0; j < ids.length; j++) {
				if(arr[i].id==ids[j]){
					arr.splice(i,1);
				}
			}
		}
		return arr;
	}
}

export default arrayHelper;



