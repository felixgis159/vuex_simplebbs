/*!
 * JIC JavaScript Library v2.0.2
 * https://github.com/brunobar79/J-I-C/
 *
 * Copyright 2016, Bruno Barbieri
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Date: Tue Jul 11 13:13:03 2016 -0400
 */

/**
 * Create the jic object.
 * @constructor
 */
 var msgpack = require("msgpack-lite");
 const jic = {
    base64ToBlob(base64Data, contentType, sliceSize){
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(base64Data.substring(base64Data.indexOf(',') + 1));
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
    },
    compress(source_img_obj, quality, output_format) {
        var mime_type = "";
        if (typeof output_format !== "undefined" && output_format == "png") {
            mime_type = "image/png";
        }else if (typeof output_format !== "undefined" && output_format == "jpg") {
            mime_type = "image/jpeg";
        }else{
            mime_type = "image/gif";
        }
        var cvs = document.createElement('canvas');
        cvs.width = source_img_obj.naturalWidth;
        cvs.height = source_img_obj.naturalHeight;
        cvs.getContext("2d").drawImage(source_img_obj, 0, 0);
        if (mime_type == "image/png" || mime_type=="image/gif") {
            var newImageData = cvs.toDataURL(mime_type);
        } else {
            var newImageData = cvs.toDataURL(mime_type, quality / 100);
        }
        var result_image_obj = new Image();
        result_image_obj.src = newImageData;
        result_image_obj.width = source_img_obj.naturalWidth;
        result_image_obj.height = source_img_obj.naturalHeight;
        return result_image_obj;
    },
    encode(obj){
        try {
            var buffer = msgpack.encode(obj);
            return  JSON.stringify(buffer);
        } catch (e) {
            return ""
        }
    }
};

export default jic;
