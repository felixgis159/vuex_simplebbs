const token = {
	getToken() {
		return localStorage.getItem("token");
	},
	removeToken() {
		return localStorage.removeItem("token");
	},
	setToken(value) {
		return localStorage.setItem("token", value);
	}
}
export default token;