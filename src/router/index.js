import Vue from 'vue';
import Router from 'vue-router';
import token from '../helper/token';

Vue.use(Router)

const router = new Router({
	mode: "history",
	routes: [
		{
			path: '/',
			name: 'home',
			component: resolve => {
				require.ensure([], () => {
					resolve(require('../module/home/home.vue'))
				}, "home");
			},
			meta: { needLogin: false }
		},
		{
			path: '/login',
			name: 'login',
			component: resolve => {
				require.ensure([], () => {
					resolve(require('../module/home/login.vue'))
				}, "home");
			},
			meta: { needLogin: false }
		},
		{
			path: '/register',
			name: 'register',
			component: resolve => {
				require.ensure([], () => {
					resolve(require('../module/home/register.vue'))
				}, "home");
			},
			meta: { needLogin: false }
		},
		{
			path: '/profile',
			name: 'profile',
			component: resolve => {
				require.ensure([], () => {
					resolve(require('../module/profile/profile.vue'))
				}, "profile");
			},
			meta: { needLogin: true }
		},
		{
			path: '/search',
			name: 'search',
			component: resolve => {
				require.ensure([], () => {
					resolve(require('../module/search/search.vue'))
				}, "search");
			},
			meta: { needLogin: false }
		},
		{
			path: '/favorite',
			name: 'favorite',
			component: resolve => {
				require.ensure([], () => {
					resolve(require('../module/favorite/favorite.vue'))
				}, "favorite");
			},
			meta: { needLogin: true }
		},
		{
			path: '/topic/:bid',
			name: 'topic',
			component: resolve => {
				require.ensure([], () => {
					resolve(require('../module/forum/topic.vue'))
				}, "topic");
			},
			meta: { needLogin: true }
		},
		{
			path: '/post/:tid',
			name: 'post',
			component: resolve => {
				require.ensure([], () => {
					resolve(require('../module/forum/post.vue'))
				}, "post");
			},
			meta: { needLogin: true }
		},
		{
			path: '/threads/:bid',
			name: 'threads',
			component: resolve => {
				require.ensure([], () => {
					resolve(require('../module/forum/topiclist.vue'))
				}, "threads");
			},
			meta: { needLogin: true }
		}
	]
});

router.beforeEach((to, from, next) => {
	if (to.meta.needLogin) {
		if (token.getToken() == null) {
			next({
				path: '/login',
				name: "login"
			})
		} else {
			next()
		}
	} else {
		next()
	}
});

export default router;
