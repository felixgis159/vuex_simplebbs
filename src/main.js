// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import router from './router';
import store from './vuex';
import { sync } from 'vuex-router-sync';
import VueResouce from 'vue-resource';
import token from './helper/token';
import FastClick from 'fastclick';
import post from './helper/post';

Vue.use(VueResouce);

Vue.config.productionTip = false
Vue.config.debug = true
sync(store, router);

Vue.http.options.emulateJSON = true;
Vue.http.options.body = "FormData";
Vue.http.interceptors.push((request, next) => {
  if(request.method == 'POST'){
    post.postLoading('block');
  }
  var jwtToken = token.getToken();
  if (jwtToken != null) {
    if (request.url.indexOf('?') >= 0) {
      request.url = request.url + '&token=' + jwtToken;
    } else {
      request.url = request.url + '?token=' + jwtToken;
    }
  }
  next(function (response) {
    post.postLoading('none');
    if (response.body.errorCode == '9999' || response.body.errorCode == '9998') {
      router.push({
        path: '/login',
        name: 'login'
      });
    }
  });
});

document.addEventListener('DOMContentLoaded', function () {
  FastClick.attach(document.body);
}, false);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router,
  store: store,
  render: (h) => h(require('./App.vue'))
})
